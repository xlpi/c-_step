﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.NumbersFormat
{
    class Program
    {
        static void Main(string[] args)
        {
            // C - curency $2,52       2,52 грн
            // c
            // D d  D6 - 123 -> 000123
            //E e  E2  2.5432 -> 2.54E0000
            // F f  F3 2,432 -> 2,43
            // G g - авто или E или F
            // N n - разделители групп
            // P p - %    х ->  100*%
            // 1 -> 100%
            // 0.98 -> 98%
            // X - > hex     X4  - занимает минимум 4 знакоместа

            // tunes format number
            // "0000"  123   -> 0123
            // # - возможное знакоместо может быть а может нет    :  ###  123,456 -> 123
            // . - разделитель
            // , - разделитель групп (тысячных)
            // % 
            // промили
            // \ - escape symbol - дальше будет как литерал :  "\####\#"  123 ->  #123#
            // {0:D'град'}    56 ->  56 град
            // {{    ->   {
            




            // если нужно передать много параметров одного типа
            // БЫЛО
            int Sum(int[] a)
            {

            }

        Sum(NewsStyleUriParser int[]{1,2,3,4,5};

    .. СТАЛО

    int Sum(params int[] a)
        {


}

    Sum(1,2,3,4,5);

    или

    int Sum(1,2,3, params int[]a)
{


}




        }
    }
}
