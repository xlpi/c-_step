﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _22MyCollections
{
    class RealeseUravnenie: IAriphmetics<RealeseUravnenie>,IIsZero 
    {
        public double Value {get; private set;}

        public RealeseUravnenie (double v)
	    {
            Value = v;
	    }

        public RealeseUravnenie Multi(RealeseUravnenie a)
        {
            return new RealeseUravnenie(Value * a.Value);
        }

        public RealeseUravnenie Multi(int a)
        {
            return new RealeseUravnenie(Value * a);
        }

        public RealeseUravnenie Substract(RealeseUravnenie a)
        {
            return new RealeseUravnenie(Value - a.Value );
        }

        public RealeseUravnenie Add(RealeseUravnenie a)
        {
            return new RealeseUravnenie(Value + a.Value);
        }

        public RealeseUravnenie Division(RealeseUravnenie a)
        {
            return new RealeseUravnenie(Value / a.Value);
        }

        public RealeseUravnenie Sqrt()
        {
            return new RealeseUravnenie(Math.Sqrt(Value));
        }

        public bool MoreZero
        {
            get
            {
                return Value > 0;
            }


        }

        public RealeseUravnenie InvertSign()
        {
            return new RealeseUravnenie(-Value);
        }

        public bool IsZero
        {
            get {
                return Value == 0;
            }
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
