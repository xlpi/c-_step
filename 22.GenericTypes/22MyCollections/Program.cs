﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections;
using System.Collections.Specialized; // spec collections

namespace _22MyCollections
{
    class Program
    {
        static void Main(string[] args)
        {
            //Arraylist (object); // сейчас уже не используется
         //   ArrayList arr = new ArrayList();
         //   Stack st = new Stack(); // last in First Out
         //   st.Contains; // is element in stack?
         //   st.Pop; // deleet from head
// stack хранит только обьекты - длинная распаковка упаковка

            // Queue  - очередь - хранит объекты

            // Hash - как map - пары ключ, значение

         //   Hashtable hash = new Hashtable();
         //   hash.Values(); // return coll keys
         //   hash.Add(object Key, object value);

         //   DictionaryEntry dic = new DictionaryEntry();

            // это все наследие старое, но все можно использовать

         //   ListDictionary ldic = new ListDictionary(); // связанный список, пары ключ-значение, для маленьктих списков
        //    HybridDictionary col1 = new HybridDictionary();// пока маленькая - ListDic. а потом HashTable
        //        NamevalueCollection;// ключи и значение  -- строки



        // fron .NET 2.0  -  GenericTypes  in System.Collections.Generic - 
        // коллекции генерируются
        // как шаблоны в С++ - разный тип можно хранить , ошибка шаблона только в рантайме, компилится на эапе выполнения
        // C# - пошли дальше и создали Generik, - позволяет создавать обощенные классы, методы, делегаты, интерфейсы  все-все




        //   G  E   N   E  R   I   C    T   Y  P   E   S
        // FOR CLASSES

            // class квадрат уравнения и я незнаю какой тп буду хранить

            try
            {


                Uravnenie<RealeseUravnenie> ur = new Uravnenie<RealeseUravnenie>(new RealeseUravnenie(2345), new RealeseUravnenie(4), new RealeseUravnenie(2));

                RealeseUravnenie x1, x2;
                ur.Calc(out x1, out x2);
                Console.WriteLine("x1 = {0}, x2 = {1}", x1, x2);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadKey();
        }

        
    }
}
