﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _22MyCollections
{
    // Generic Types template
    class Uravnenie<T> where T : IIsZero, IAriphmetics<T>
    {// T:class, new() - ограничения, - должен быть классом, должен иметь конструктор по умолчанию
        T _a; // default value в зависимости от типа
        T _b;
        T _c;

        public Uravnenie(T a, T b, T c)
        {
            // чтобы проинитить то использ  _a = default(T);
            _a = a;
            _b = b;
            _c = c;
        }

        public void Calc(out T x1, out T x2)
        {
            // if (a == 0) //  error не знаем тип, есть ли оператор сравнения с интом
            // есть выход через общий контракт для всех - интерфейс
            // теперь можем сравнивать на ноль
            if (_a.IsZero)
            {
                throw new Exception("ОШИБКА!!! a == 0");
            }

            //  calc
            // T d = b * b - 4 * a * c; // не знаем как умножать, делить....делаем интерфейс IAriphmetics
            T d = _b.Multi(_b).Substract(_a.Multi(4).Multi(_c));

            if (d.MoreZero)
            {
                // x1 = (-b + sqrt(d) )  /   2*a
                // x2 = (-b - sqrt(d) )  /   2*a
                x1 = (_b.InvertSign().Add(d.Sqrt())).Division(_a.Multi(2));
                x2 = (_b.InvertSign().Substract(d.Sqrt())).Division(_a.Multi(2));
            }
            else if (d.IsZero)
            {
                // x1 = x2 = -b / 2*a
                x1 = _b.InvertSign().Division(_a.Multi(2));
                x2 = x1;
            }
            else
            {
                // нет корней

                throw new Exception("ОШИБКА!!! Нет корней!");
            }

        }
    }
}
 

