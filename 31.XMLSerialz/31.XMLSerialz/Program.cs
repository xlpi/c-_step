﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Xml.Serialization;
using System.Xml;

namespace _30.Serials
{
    namespace _31.XMLSerialz
    {

        class Program
        {
            static void Main(string[] args)
            {
                MyType o = new MyType() { Field1 = "._", Field2 = 23 };
                

                // Serialz.  -  поумолчанию - только public
                XmlSerializer xs = new XmlSerializer(typeof(MyType));
                string XmlFile = "MyXML_ser.xml";

                using (FileStream s = new FileStream(XmlFile, FileMode.Create))
                {
                    using (XmlWriter w = new XmlTextWriter(s, Encoding.Unicode))
                    {
                        xs.Serialize(w, o);
                    }
                }

                // Deserialz
                MyType b = new MyType();
                using (FileStream fs = new FileStream(XmlFile, FileMode.Open))
                {
                    using (XmlReader r = XmlReader.Create(fs))
                    {
                        b = (MyType)xs.Deserialize(r);
                    }
                }

                if (o.Field1 == b.Field1 && o.Field2 == b.Field2)
                {
                    Console.WriteLine("Deserialization OK!");
                }
                else
                {
                    Console.WriteLine("Deserilization bad");
                }


            }
        }
    }
}