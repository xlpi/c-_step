﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml.Serialization;
// save состояния обекта сериализация   -  восстановление - десириализация
// bin  or xml - by NET
namespace _30.Serials
{
    [Serializable]
    public class MyType : ISerializable
    {
        public MyType()
        {
        }

        public MyType(SerializationInfo info, StreamingContext context)
        {
            _field1 = (string)info.GetValue("f1", typeof(string));
            _field2 = (int)info.GetValue("f2", typeof(int));
        }

        private string _field1;
        private int _field2;

        [XmlArray]
        int[] arr = { 5, 4, 3, 4, 5, 6, 6 };

        [XmlAttribute] 
        public string Field1 { get { return _field1; } set { _field1 = value; } }
        public int Field2 { get { return _field2; } set { _field2 = value; } }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("f1", _field1, typeof(string));
            info.AddValue("f2", _field2, typeof(int));

        }



    }
}
