﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16.Complex
{
    class Complex
    {
        public double Re
        {
            get;
            set;
        }

        public double Im
        {
            get;
            set;
        }

        //constructor
        public Complex(double a, double b)
        {
            Re = a;
            Im = b;
            
        }

      

        public static Complex operator +(Complex a, Complex b)
        {

            return new Complex( a.Re + b.Re, a.Im + b.Im );
        }

        public static Complex operator -(Complex a, Complex b)
        {

            return new Complex(a.Re - b.Re, a.Im - b.Im);
        }

        public static Complex operator *(Complex a, Complex b)
        {

            return new Complex(a.Re*b.Re - a.Im*b.Im, a.Re*b.Im + a.Im*b.Re);
        }

        public static Complex operator /(Complex a, Complex b)
        {

           // return new Complex( , );  дописать
        }



    }
}
