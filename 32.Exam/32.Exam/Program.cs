﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

// + run - see start directory
//      Change directory
// + cd home — перейти в директорию '/home'   -   relative
// cd d:\PrjVisul2013\home                     - absolute
// + cd \ - go to root
// + cd .. — перейти в директорию уровнем выше
// cd ../.. — перейти в директорию двумя уровнями выше
// + cd - — перейти в директорию, в которой находились до перехода в текущую директорию

// ...+ dir  |  dir /w  | dir /p   -  /w - по колонкам    / p - постранично, заполнили страницу, нажали пробел - следующая страница
// del "target"|   del "target" /y  |- удаляем все не спрашивая
// copy "from" "to"|  copy "from" "to" /y |  -  что куда  /y - не спрашивая, а если флага нет то спрашиваем   C:\*  - все файлы, C:\Win\File.txt
// move "from" "to"|  move "from" "to" /y  |
// md "name"- createDir - в текущей директории




namespace _32.Exam
{
    class Program
    {
        static private String Command;
        static private String Key;
        static String previusDirectory;

        static bool isLocalDirectories(string currentDir, string key){
            bool result = false;
            string[] localDirectories = Directory.GetDirectories(currentDir);
            foreach(string i in localDirectories){
                if(i==currentDir+key){
                    result = true;
                    return result;
                }
            }
            return result;
            
        }

        public static void Main(String[] args)
        {
             
            
            //for (int i = 0; i < args.Length; i++)
            //{
            //    //Console.WriteLine(args[i]);
            //    if (i == 0)
            //    {
            //        Command = args[i];
            //    }

            //    if(i==1)
            //    {
            //        Key = args[i];
            //    }
            //}

            
            //    if (Command != null && Key != null)
            //    {
            //        Console.WriteLine("Выполнение комманды...\n" + "Command: " + args[0] + "\nKey: " + args[1]);
            //    }
            //    else if (Command != null)
            //    {
            //        Console.WriteLine("Выполнение комманды...\n" +"Command: "+ args[0]);
            //    }

            int countSplit = 2;
            int columns = 3;
            String parentDirectory;
            String currentDirectory;

            bool initCurrent = false;

            Console.WriteLine("\t\t= = = Exam task, by Korchagin Alexander, group s15pz3, IT STEP, 2015 = = =");
            Console.WriteLine();

            while (true)
            {
                try
                {
                    // cw - current direcory
                    if (initCurrent)
                    {
                        currentDirectory = Directory.GetCurrentDirectory();

                        Console.WriteLine();
                        Console.WriteLine(currentDirectory);
                    }
                    else
                    {
                        currentDirectory = Directory.GetDirectoryRoot(Directory.GetCurrentDirectory());
                        previusDirectory = currentDirectory;
                        Directory.SetCurrentDirectory(currentDirectory);
                        Console.WriteLine(currentDirectory);
                        initCurrent = true;
                    }

                    String input = Console.ReadLine();

                    String[] inputArr = input.Split(" ".ToCharArray(), countSplit);

                    for (int i = 0; i < inputArr.Length; i++)
                    {
                        if (i == 0)
                        {
                            Command = inputArr[i];
                        }

                        if (i == 1)
                        {
                            // check key
                            Key = inputArr[i];
                            //if (Key.Length < 2 || Key.Length > 2)
                            //{
                            //    Console.WriteLine("Ошибка ввода ключа!"); continue;
                            //}
                        }
                    }
                    //if (Key != null)
                    //{
                    //    if (Key.Length < 2 | Key.Length > 2) { continue; }
                    //}
                    //else if(Command == null)
                    //{
                    //    continue;
                    //}
                    //else { continue; }

                    if (Command == null || Command == "\n") { continue; }


                    //debug
                    //if (Command != null && Key != null)
                    //{
                    //    Console.WriteLine("Выполнение комманды...\n" + "Command: " + inputArr[0] + "\nKey: " + inputArr[1]);
                    //}
                    //else if (Command != null)
                    //{
                    //    Console.WriteLine("Выполнение комманды...\n" + "Command: " + inputArr[0]);
                    //}


                    if (Command == "help")
                    {
                        Console.WriteLine("\t\t===HELP===");
                       // Console.WriteLine("Avialable commands:");
                        Console.WriteLine("\"help\" - help ");
                        Console.WriteLine("\"cd ..\" - change current to parent");
                        Console.WriteLine("\"cd - \"- change current to previus");
                        Console.WriteLine("\"cd local Dir\" - go to local");
                        Console.WriteLine("\"cd - \\ - go to root");
                        Console.WriteLine("\"dir - /p - print list pages");
                        Console.WriteLine("\"dir - /w - print by columns");
                        Console.WriteLine();
                    }

                    ///      cd
                    if (Command == "cd" && Key == "..")
                    {
                        // check key->path is?
                        parentDirectory = Directory.GetParent(currentDirectory).ToString();
                        previusDirectory = currentDirectory;
                        Directory.SetCurrentDirectory(parentDirectory);
                    }

                    if (Command == "cd" && Key == "-")
                    {
                        Directory.SetCurrentDirectory(previusDirectory);
                        previusDirectory = currentDirectory;
                        
                    }

                   string[] localDirectories = Directory.GetDirectories(currentDirectory);

                   if (Command == "cd" && isLocalDirectories(currentDirectory, Key))
                   {
                       string newDirectory = currentDirectory + Key;
                       Directory.SetCurrentDirectory(newDirectory);
                       previusDirectory = currentDirectory;
                   }


                   if (Command == "cd" && Key == "\\")
                   {
                       currentDirectory = Directory.GetDirectoryRoot(Directory.GetCurrentDirectory());
                       previusDirectory = currentDirectory;
                       Directory.SetCurrentDirectory(currentDirectory);
                   }

                   if (Command == "dir" && (Key == null || Key == "/p"))
                   {
                      // string[] localDirs = Directory.GetDirectories(currentDirectory);
                       foreach (string dir in localDirectories)
                       {
                           String[] temp = dir.Split("\\".ToCharArray());
                           String output = temp[temp.Length - 1];
                           Console.WriteLine("\t"+output);
                       } 
                   }

                   if (Command == "dir" && (Key == "/w"))
                   {
                       //string[] localDirs = Directory.GetDirectories(currentDirectory);
                       Console.Write("  ");
                       int currentLine = 0;
                       foreach (string dir in localDirectories)
                       {
                           String[] temp = dir.Split("\\".ToCharArray());
                           String output = temp[temp.Length - 1];
                           Console.Write(output+"\t\t\t");   ///    <============================================ !!!
                           currentLine++;
                           if (currentLine == columns)
                           {
                               Console.WriteLine();
                               currentLine = 0;
                           }
                           
                       }
                   }






                    // reset command
                    Command = null; Key = null;
                }

                catch{
                    continue;
                }


            } // while(true)


                
                // обработка команды
            // print current directory \n
            // command

        }
    }
}
 