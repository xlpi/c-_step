﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20.Structss
{
    struct Date
    {
        private bool BeforeEra = false;
        private bool yearUP = false;
        public int Day { get; private set;}
        public int Month { get; private set; }
        public int Year { get; private set;}

        public Date(int d, int m, int year)
        {
            if(year>=0) {
                Year = year;
                if(m>0&&m<13) {
                    Month = m;
                    if(Month==1 || Month ==3 || Month ==5 || Month == 7 || Month == 8 || Month ==10 || Month ==12  ){
                        // 31 days
                        if(d>0 && d<32) {
                            Day = d; return;
                        } else {
                            throw new ArgumentOutOfRangeException("In " + Month + " month must be 1-31 days");
                        }

                    } else if(Month ==4 || Month ==6 || Month == 9 || Month == 11){
                        // 30 days
                        if(d>0 && d<31) {
                            Day = d; return;
                        } else {
                            throw new ArgumentOutOfRangeException("In "+ Month  + " month must be 1-30 days");
                        }

                    } else if(Month ==2 && Year%4 ==0 && Year %100 !=0 ){
                        // february 29
                        if(d>1 && d<30) {
                            Day = d; return;
                        } else {
                            throw new ArgumentOutOfRangeException("In "+Year+" year in February must be 1-29 days");
                        }

                    } else if(d>1 && d<29) {
                        // feb. 28
                        Day = d; return;
                    } else {
                        throw new ArgumentOutOfRangeException("In "+Year+" year in February must be 1-28 daya");
                    }

                } else {
                    throw new ArgumentOutOfRangeException("Month must be 1-12"); 
                  }
            } else if (Year<0) {
               BeforeEra = true;
            }
        }
        /*
        public static Date operator+(Date d, int days){
            Date result = new Date();
          // 30 01 2015
          //  +
          // 05 02 2015
           // 35 02 2015  35-(Month-1).days, MOnth +1 ;
            // добавляем дни 


            return result;

        }

        
        public static Date operator - (Date, int);
        public static int operator - (Date one, Date two) {
            int days;
            if(one>two){

            } else if (one<two) {

            }

         return days;
        }
        public static bool operator < (Date, Date);
        public static bool operator <= (Date, Date);
        public static bool operator > (Date, Date);
        public static bool operator >= (Date, Date);
        public static bool operator == (Date, Date);
        public static bool operator != (Date, Date);

         */

        public override string ToString()
        {
            if(BeforeEra) {
                return "Date YY.MM.DD b.our.age: "+Year+"."+Month+"."+Day+" b.our.age.";
            } else {
                return "Date YY.MM.DD: "+Year+"."+Month+"."+Day;
            }
        }

    };


    // toString - переопределить
}
