﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SquareX
{
    class Equation
    {
        
        double _a;
        double _b;
        double _c;

        public Equation(double a = 1.0, double b = 0.0, double c = 0.0)
    {
            _a = a;
            _b = b;
            _c = c;

    }

        public bool Calculate(out double X1, out double X2)
        {
            if (_a != 0)
            {
                double D = _b * _b - 4 * _a * _c;

                if (D > 0)
                {
                    X1 = (-_b - Math.Sqrt(D)) / 2 * _a;
                    X2 = (-_b + Math.Sqrt(D)) / 2 * _a;
                    return true;
                }
            }
           


            X1 = 0.0;
            X2 = 0.0;
            return false;

        }


    }
}
