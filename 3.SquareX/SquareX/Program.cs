﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SquareX
{
    class Program
    {
        static void Main(string[] args)
        {
           // Equation e1 = new Equation(); // сейчас класс заполнился параметрами поумолчанию из пользовательского конструктора

            double X1;
            double X2;
            double intA;
            double intB;
            double intC;
            bool result; 

            Console.WriteLine("Please input coefficient of suare equation:");
            Console.WriteLine("ax2+bx+c   :");
            Console.WriteLine("Please input a");
            String inputA = Console.ReadLine();
            Console.WriteLine("Please input b");
            String inputB = Console.ReadLine();
            Console.WriteLine("Please input c");
            String inputC = Console.ReadLine();

            //convert
            intA = Convert.ToDouble(inputA);
            intB = Convert.ToDouble(inputB);
            intC = Convert.ToDouble(inputC);

            Equation e1 = new Equation(intA,intB,intC);

            result = e1.Calculate(out X1, out X2); // вернуть иксы и bool - о возможности решения

            if (result == true)
            {
                Console.WriteLine("X1 = {0}, X2 = {1}", X1, X2);
            }
            else
            {
                Console.WriteLine("Error calculate");
            }


        }
    }
}
