﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.Time
{
    class Program
    {
        static void Main(string[] args)
        {
            // use standart DateTime formating
            var d = DateTime.Now;
            Console.WriteLine(string.Format("{0:D}",d));
            Console.WriteLine(string.Format("{0:d}", d));
            Console.WriteLine(string.Format("{0:f}", d));
            // tunes formats
            Console.WriteLine(string.Format("{0: ddd dd.yyyy.MM h:mm:ss K}", d));
        }
    }
}
