﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadiusCircle_L_S
{







    // модификаторы доступа
    // public - доступ вне сборки, вне namtspace
    // internal - закрытый, но доступгый внутри нашей сборки
    // private - закрытый, по-умолчанию создаеться, - доступ только внутри нашего namespace

    // public _static_  -  crate obj of it class - нельзя
    // ...abstract  -  not create obj of this class
    // ...sealed.. - not наследоваться - последний, если нужно наследоваться как вариант используем композицию 

    public static class Human
    {
        // поля, модификатор достуа по умолчаниею приват - private, public,protected public
//private
//internal
//protected
        // имена обычно такие - _nameCamelCase;


        // STATIC fields
        private static string B; // поле не принадлежит объектам класса, а принадлежит классу, гарантирует что поле будет создано один раз для всех объектов класса Human, обращение Human b = new Human(); b.B;


        // for ref types по лумолчанию - null, boolean - false, DateTime - 
        private string _name;
        private string _surname;
        private int _age; // 0 def


        // READONLY in constructor
        private readonly int MinAge;

        public Human()
        {
            MinAge = 5;
        }

       

        // METHODS - name UPPERcase

        public int GetAge()
        {
            return this._age;

        }

        public void SetAge()
        {
            this._age = _age;
        }


        // from v.4.5 .NET reloads method - появился параметр по умолчанию
        // reload methods - обязательно возвращаем один и тотже тип!!!
        public void SetNameSurname(string name, string surname = "")   // параметр по умолчанию если surname не будет то будет пусто
        {
            this._name = name;
            this._surname = surname;
        }

        // теперь этот метод не нужен, так как есть параметр по умолчанию
        public void SetNameSurname(string name)
        {
            SetNameSurname(name, string.Empty);
        }

        public void SetNameSurname(int age)
        {
            this._age = age;
        }


        // STATIC METHODS
        public static Human CreateHuman(string name,surname )
        {

        }

        // C# 6.0 VS 2015
        using System.Console;

        // call static metyhod of Console
        WriteLine(); // ok
        

        //  СВОЙСТВА
        public string Name
        {
            get { return this._name; }  // внутри можем делать проверки
            set{this._name = value;}

        }

        // Анонимный СВОЙСТВА если не нужны проверки
        public DateTime birthDay
        {
            get;
            set;
        }

        // для get set них можно делать свои модификаторы доступа
        public DateTime birthDay
        {
            get;
            private set;
        }


        // конструкторы, методы и свойства
        // public,private, protected - тоько наследникам, internal - внутри сборки только доступ
        public Human()    // уже СОЗДАН таблица виртуальных методов
        {

        }
        // constr by default будет если нет явного
        // если есть явный - то по умолчанию не будет

        // .Net в одном констр можно вызвать другой this()
        public Human(string name, string surname)
            :this()
        {

        }

        
        // STATIC constr
        // гарантия на уровне языка что вызовится в однопоточном режиме - атомарный доступ
        // статик поля будут проинит каждая перед своим обращением к ней, а чтобы проинит все сразу преждевременно используем статик конструктор

        static Human()
        {

        }


        // КОНСТРУКЦИИ ЯЗЫКА отличия
        // string in C# - const
        






        // new return exeption (NULL)  когда не выделил ПАМЯТЬ






        // дустр нет, за нас работает сборщик мусора
        //как вариант есть финалайзер
        // будет вызван сборщиком мусора но когда никто не знает
        ~Human() // <- финалайзер
        {

        }

    }
}




switch(int/string)
    {
        case const1:
            goto const2; // для перехода сразу на const2
        break;
        case const2:
        case const3:....break; // break для обоих

        default:
        break;
    }




// только для чтения, для коллекции
foreach(type item in collection)
{


}

// если item ссылочный тип то его можно изменить



//ПЕРЕДАЧА параметров - все передаются по значению - создаются копии обектов
// ссылочные типы - копируется ссылка 

еслипередаем перемнную класса - то передается копия, но изменения полей происходят и в оригинале обекта классаа

Передача по ссылке

void f(Human h, ref int a)
{
h = new Human();
h.Name = "Vasya";
a = 5;
}


//где-то
Human H = new Human();
int b =3;
f(H,ref b);
int c;
f(H,out c)
c==5;



ref - для передачи В метод по ссылке
out - для передачи ИЗ метода по ссылке(меняем оригинал), своего рода return

 OUT
void f(Human h, out int a)
{
h = new Human(); // другой Human()
h.Name = "Vasya";
a = 5;
}


//где-то
Human H = new Human();

f(H,out c)
c==5;