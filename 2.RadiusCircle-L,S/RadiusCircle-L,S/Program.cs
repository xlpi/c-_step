﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RadiusCircle_L_S
{
    class Program
    {
        static void Main(String[] args)
        {
            Decimal result;
            Console.WriteLine("Please input radius 0,4 - rus local:");
            // String input = new String();
            String input = Console.ReadLine();


            //calc L circle
            result = Convert.ToDecimal(input);
            Console.WriteLine("L of cirle = {0}", 2*(Decimal)Math.PI*result);
            
            //calc square of circle
            Console.WriteLine("Square of circle = {0}",(Decimal)Math.PI*result*result);
            // getNowThread.Local.GetSet - менять разделитель

            /*
            Human A = new Human();
            A.GetAge();
            A.SetAge(5);

            // лучше так
            A.GetAge = 5;
            */
            // для этого используем "свойства"


        }
    }
}
