﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadiusCircle_L_S
{
    class Human2
    {
        private string _name;
        private string _surname;
        private int _age; // 0 def
        public const int A = 123; // пример константы
        public readonly int MaxAge = 100; // onlyREAD

        public string Name
        {
            get
            {
                return this._name;
            }

            private set
            {
                this._name = value;
            }
        }
    }
}
