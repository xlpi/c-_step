﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
//xmlDoc.DocumentElement; // ret root doc
            //xmlDoc.DocumentType; // type of doc
            //xmlDoc.InnerText; // ret text
            //xmlDoc.IsReadOnly; // only for read
            //xmlDoc.GetEnumerator();
            //xmlDoc.AppendChild(); // add some child node
            //xmlDoc.Clone(); // clone node без дочернего
            //xmlDoc.CloneNode(true); // клон + дерево дочерних
            //xmlDoc.CreateAttribute();
            //xmlDoc.CreateComment();
            //xmlDoc.CreateElement();
            //xmlDoc.GetElementById();
            //xmlDoc.GetElementsByTagName();
            //xmlDoc.InsertAfter();
            //xmlDoc.InsertBefore();
            //xmlDoc.Load(); // from adrtess file or... to stream? file, textWriter...
            //xmlDoc.LoadXml();  // to adress
            //xmlDoc.RemoveAll();
            //xmlDoc.RemoveChild();
            //xmlDoc.Save(); // to stream? file, textWriter...
            //xmlDoc.Validate(); // согласно некой схемы
            //xmlDoc.WriteContentTo(); // save to xmlWriter

            //// events
            //xmlDoc.NodeChanged();
            //xmlDoc.NodeInserted();
            //xmlDoc.NodeRemoving();
            //<people>
            //    <human sex=" ">
            //        <name>
            //        <surname>
            //    </human>
            //</people>
namespace _28.XMLki
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument doc = new XmlDocument();
            
            var pi = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            XmlElement root = doc.CreateElement("people");
            doc.AppendChild(root);

            var h = doc.CreateElement("human");
            var s = doc.CreateAttribute("sex");
            s.Value = "male";
            h.Attributes.Append(s);
            root.AppendChild(h);
            var n = doc.CreateElement("name");
            n.InnerText = "Николай";
            h.AppendChild(n);
            ////////////////////////////////////////////////////
            var h1 = doc.CreateElement("human");
            var s1 = doc.CreateAttribute("sex");
            s1.Value = "male";
            h1.Attributes.Append(s1);
            root.AppendChild(h1);
            var n1 = doc.CreateElement("name");
            n1.InnerText = "Сергей";
            h1.AppendChild(n1);
            ////////////////////////////////////////////////////
            var h3 = doc.CreateElement("human");
            var s3 = doc.CreateAttribute("sex");
            s3.Value = "male";
            h3.Attributes.Append(s3);
            root.AppendChild(h3);
            var n3 = doc.CreateElement("name");
            n3.InnerText = "Вася";
            h3.AppendChild(n3);
            ////////////////////////////////////////////////////
            var h2 = doc.CreateElement("human");
            var s2 = doc.CreateAttribute("sex");
            s2.Value = "female";
            h2.Attributes.Append(s2);
            root.AppendChild(h2);
            var n2 = doc.CreateElement("name");
            n2.InnerText = "Дарья";
            h2.AppendChild(n2);
            ////////////////////////////////////////////////////

            doc.Save("MyXML.xml");
            // selest Nodes  - выбрать всех людей  =  Вася
            XmlNodeList findNode = doc.SelectNodes("//human[name='Вася']");

            foreach (XmlNode list in findNode)
            {
                Console.WriteLine(list.Name + ":" + list.InnerText.ToString());
                //Console.WriteLine(list.Name + ":" + list.InnerText); // лучше и так работает
            }


        }
    }
}
