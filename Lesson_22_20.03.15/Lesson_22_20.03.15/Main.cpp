#include <iostream>
#include <ctime>

using namespace std;

/*
void converteDeegreesToRadians (double&angle);
void converteRadiansToDeegrees (double&angle);
void converteKmToMiles (double&distance);
void converteMilesToKm (double&distance);
void converteCelciumToKelvin (double&temperature);
void converteKelvinToCelcium (double&temperature);

void main ()
{
	double angle, distance, temperature;
	cout << "Input angle = ";
	cin >> angle;
	cout << "Input distance = ";
	cin >> distance;
	cout << "Input temperature = ";
	cin >> temperature;
	converteDeegreesToRadians (angle);
	cout << "angle Radians = " << angle << endl;
	converteRadiansToDeegrees (angle);
	cout << "angle Deegrees = " << angle << endl;
	converteKmToMiles (distance);
	cout << "distance Miles = " << distance << endl;
	converteMilesToKm (distance);
	cout << "distance Km = " << distance << endl;
	converteCelciumToKelvin (temperature);
	cout << "temperature Kelvin = " << temperature << endl; 
	converteKelvinToCelcium (temperature);
	cout << "temperature Celcium = " << temperature << endl;
}

void converteDeegreesToRadians (double&angle)
{ angle = angle * 3.1415 / 180; }
void converteRadiansToDeegrees (double&angle)
{ angle = angle * 180 / 3.1415; }
void converteKmToMiles (double&distance)
{ distance = distance / 1.6093; }
void converteMilesToKm (double&distance)
{ distance = distance * 1.6093; }
void converteCelciumToKelvin (double&temperature)
{ temperature = temperature + 273.15; }
void converteKelvinToCelcium (double&temperature)
{ temperature = temperature - 273.15; }
*/

//���������� �������
int array[20];

void printArray ();
void putArray ();
void selectionSort ();
void bubbleSort ();

void main ()
{
	putArray ();
	printArray ();
	selectionSort ();
	printArray ();
	putArray ();
	printArray ();
	bubbleSort ();
	printArray ();
}

void putArray ()
{
	for (int i=0; i<20 ; i++)
	{
		array[i] = rand() % 10;
	}
}

void printArray ()
{
	for (int i=0; i<20 ;i++)
	{
		cout << array[i] << " ";
	}
	cout << endl;
}

void selectionSort ()
{
	for (int i=0;i<19;i++)
	{
		int min=i;
		for (int j=i+1;j<20;j++)
		{
			if (array[j]<array[min]) min=j;
		}
		swap(array[i],array[min]);
	}
}

void bubbleSort ()
{
	for (int i=0;i<20;i++)
	{
		for (int j=0;j<20;j++)
		{
			if (array[j]>array[i]) swap(array[i],array[j]);
		}
	}
}