﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security;

namespace FilesAccesTime
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                while(true)
                {
                    try
                    {
                    Menu.MainMenu();
                    }
                    catch(FileNotFoundException ex){
                Console.WriteLine("File not found"+ex.Message);
            }
            catch (PathTooLongException ex)
            {
                Console.WriteLine("File name or path is too long"+ex.Message);
            }
            catch (DirectoryNotFoundException ex)
            {
                Console.WriteLine("Directory not find"+ex.Message);
            }
            catch(IOException ex){
                Console.WriteLine("Input\"output error"+ex.Message);
            }
            catch(ArgumentNullException  ex){
                Console.WriteLine("File name is empty"+ex.Message);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("File mode is wrong");
            }
            catch(ArgumentException  ex){
                Console.WriteLine("File name is wrong"+ex.Message);
            }
            catch (SecurityException ex)
            {
                Console.WriteLine("File access security is wrong");
            }

            catch (NotSupportedException ex)
            {
                Console.WriteLine("Not find drive");
            }
                }
            }
            

        catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
    }
}
