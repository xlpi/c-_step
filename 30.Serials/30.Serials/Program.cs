﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

namespace _30.Serials
{
    class Program
    {
        static void Main(string[] args)
        {
            // obj for serializ.
            MyType o = new MyType() { Field1 = "._", Field2 = 23 };


            //   Serialization
            string filename = "MyType.dat";
            IFormatter f = new BinaryFormatter(); // ser to bin

            using (FileStream streamOut = new FileStream(filename, FileMode.Create))
            {
                f.Serialize(streamOut, o);
                streamOut.Close();
            }

            // Deserialization
            IFormatter f2 = new BinaryFormatter(); // ser to bin
            MyType b = new MyType();
            using (FileStream streamIn = new FileStream(filename, FileMode.Open))
            {
                b = (MyType)f2.Deserialize(streamIn); // ret. obj and приводим к нашему типу
                streamIn.Close();
            }

            if (o.Field1 == b.Field1 && o.Field2 == b.Field2)
            {
                Console.WriteLine("Deserialization OK!");
            }
            else
            {
                Console.WriteLine("Deserilization bad");
            }


        }
    }
}
