﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14.PrivedenieObjects
{
    //   // OVERLOADS    point on square
    class Cpoint
    {
        int _x;
        int _y;

        // properties X
        public int X
        {
            get { return _x; }
            set { _x = value; }
        }

        // properties Y
        public int Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public static Cpoint operator ++(Cpoint p)
        {
            p._x++;
            p._y++;
            return p;
        }

        public static Cpoint operator +(Cpoint a, Cpoint b)
        {
            return new Cpoint() { X = a.X + b.X, Y = a.Y + b.Y }; 
        }

        //    *
        // скалярное произведение -  умножение точки на точку
        // z = x1x2 + y1y2;



        public static bool operator true(Cpoint p)
        {
            return p.X > 0 && p.Y > 0;
        }

        public static bool operator false(Cpoint p)
        {
            return p.X < 0 && p.Y < 0;
        }
       




        // explicit - явное преобразование
        public static explicit operator int(Cpoint p)
        {
            return (int)Math.Sqrt(p._x * p._x + p._y * p._x);
        }


        // implicit - неявное
        public static implicit operator double(Cpoint p)
        {
            return Math.Sqrt(p._x * p._x + p._y * p._x);
        }



        // - operator indexator   [  ]   -  свойство, безимени, 

        public int this[int index]
        {
            get
            {
                if (index == 0)
                {
                    return _x;
                }
                else if (index == 1)
                {
                    return _y;
                }
                else
                {
                    throw new ArgumentOutOfRangeException();
                }

            }

            set {
                if (index == 0)
                {
                    _x = value;
                }
                else if (index == 1)
                {
                    _y = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException();
                }

            }

        }
        

    }


    class Program
    {
        static void Main(string[] args)
        {
            // Class A :  B    // B - наследник
            /*
            B b = new A();
            B c = new B();
            A a = (A)b; // ok
            A a1 = // наоборот не работает будет исключение

                a.GetType - получим тип объекта

                    if(a is A)   // проверяем а - это объект типа А? - true/false

            **/
          
            Cpoint p = new Cpoint() {X =2, Y = 4};
            p++;
            ++p;

            Cpoint a = new Cpoint() {X =2, Y = 4};
            Cpoint b = new Cpoint() { X = 3, Y = 5 };
            Cpoint c = new Cpoint() ;
            c = a + b;
          
            /*
            Object.Equals // comparew linkes    -  для объектов
                value types .Equals // compare fields    -  для структур

                    NotEquals;
            */


            // reload false, true

            if (c)   // true - I квадрант, III - false 
            {

            }



            // приведение (int)Cpoint

            int in1 = 5 + (int)c;

            // приведение (double)Cpoint


            // indexator
            c[0] = 5;
            c[1] = 7;

            // c[2]  - нужно еще перегрузить

        }
    }
}
