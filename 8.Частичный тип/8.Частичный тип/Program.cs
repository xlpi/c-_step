﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8.Частичный_тип
{
    class Program
    {
        static void Main(string[] args)
        {
            // только на шарпе - если файл класса большой - его можно разбивать на несколько файлов создав в каждом файле частичный тип
            // для двух человек
            // file Human.cs
            public partial class Human
            {
                public void DoMagic()
                {
                }
            }

            // file Human1.cs
        partial class Human
        {
            public void GoLunch()
            {
            }
        }

        // частичные методы только в частичных классах
        // классы должны быть в одном наймспейсе
        // модификатор доступа в разных частях должен олдинаковый
        // если seled то и вторая тоже
        // если наследуются от класса а, то и другая часть тоже будет
        // поля не дожны совпадать
        // частичные структуры и част интерфейсы
        // partial struct  - partial должно нах возле слова класс

        //         частичны Methods
         // file Human.cs
            public partial class Human
            {
                public void DoMagic()
                {
                }
                partial void GoLunch()   -  только void, только (ref ), может быть static
                {

                }
            }

            // file Human1.cs
        partial class Human
        {
            partial void GoLunch()
            {а тут реализация
            }
        }

    // WinForms   - можно использовать частичную реализацию - дизайнер пишет один файл, а вы другой файл

    // les 2 END

        }
    }
}
