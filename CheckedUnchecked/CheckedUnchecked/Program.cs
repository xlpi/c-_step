﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckedUnchecked
{
    class Program
    {
        static void Main(string[] args)
        {
            //project properties: check - ON/OFF
            // if ON - will be Exeption when be overflow

            /*
            Byte b = 100;
            Byte c = (byte)(b + 200);
            Console.WriteLine("result overflow {0}",c);

            //use exeption to control overflow exeption
            */
            Byte b1 = 100;
            try
            {
                unchecked    // control, if write  unchecked = ignor overflow
                    //unchecked
                {
                    b1 = (byte)(b1 + 200); // 200 - int, -> (byte)
                }
            }
            catch (OverflowException e)
            {
                Console.WriteLine(e.Message);
                
            }

            Console.WriteLine("result overflow {0}", b1);

        }
    }
}
