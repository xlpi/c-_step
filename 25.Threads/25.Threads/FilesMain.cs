﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace _25.FilesMain
{
    class FilesMain
    {
        static void Main(string[] args)
        {
            // Stream st = new Stream();

            //st.CanTimeout; // есть ли ТаймАут
            //st.CanWrite; // можем ли писать в поток
            //st.ReadTimeout; // 
            //st.WriteTimeout;

            ///// Methods
            ///// 

            //st.Close(); // closecurrent threas and call dispose\
            //st.CopyTo; // copy thread to threads
            //st.CopyToAsync; // -//- asynhronno; не ожидаем завершения предыдущей операции
            //st.BeginRead; // begin operac asyns read
            //st.ReadAsync; // new --//--
            //st.Flush; // очищает буфер и загонфет все в конечный результат,
            //st.FlushAsync; // -- // -- для передачи очень маленьких 5 байт ... по сети...
            //st.ReadByte;
            //st.ReadAsync; // считать с потока байт
            //st.SetLength; // можно переопределить длиннну потока
            //st.WriteAsync; // записываем некий массив в поток
            //st.WriteByte; // записать некий байт в поток

            //  от негшо наследуются все остальные

            // далее будем учить обертки но имеюю что-то свое  например BinaryReader
            // 

            /////             ЗАДАЧА   -  с файлами работа с потоками

            // FileStream fs = new FileStream(string filepath, FileMode.Append, FileAccess.ReadWrite, FileShare.ReadWrite);


            // D:\s15pz3\Korchagin\25.Threads\25.Threads\bin\Debug\data.txt
            using (FileStream fs = new FileStream("data.txt", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                Console.WriteLine("Input string line");
                string s = Console.ReadLine();
                Byte[] b = Encoding.Unicode.GetBytes(s); // take array of bytes b from Unicode s

                fs.Write(b, 0, b.Length); // пишем что, с какого смещения - , и сколько
                fs.Flush(); // to drive
                fs.Seek(0, SeekOrigin.Begin); // ищем 

                Byte[] r = new Byte[fs.Length]; // читаем массив байт равный длинне файла

                // read

                fs.Read(r, 0, r.Length);

                String readSt = Encoding.Unicode.GetString(r);

                Console.WriteLine("Correct read : {0}", s.Equals(readSt) );

                /// 5 class для папок Directory - static, DirectoryInfo - instance of Folder+path, 
                /// // files: File, FileInfo   - https://msdn.microsoft.com/ru-ru/library/system.io.fileinfo(v=vs.110).aspx
                /// DirectoryInfo(path, (virtual path), less7, page 25
                
                // FileSystemObject - base for FileInfo, DirectoryInfo etc.
                



            }


        }
    }
}
