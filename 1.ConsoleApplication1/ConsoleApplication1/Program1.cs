﻿using System;

// этипока можем удалить
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// referance  -  our includes
// now we need only System include (using)
// Linq - язык запросов
//appConfig - как запускать нашу прогу
// System.Int32 - чтобы сократить пишем using System;  потом Int32 просто


namespace XLPI
{
    class Program1
    {
        static void Main(string[] args)
        {
            //Console.Read(); //Console - press F12 - rede methods of Consolereturn one symbol
            //int a;
            //char c = (char)a; // int To char
           // Console.ReadLine(); // reade all string before ENTER
            Console.WriteLine("Hi, i'm C#");
            Console.WriteLine("What is your name?");
            String name = Console.ReadLine();

            //Console.Write(); // write to Console - 18-19 method (reloads);  NOT +ln
            Console.WriteLine("Hello, {0}!",name); // + ln
            // spipped - it is   press C,w,tab,tab
            Console.WriteLine();
            
            // formated OUTPUT   "{0}, {2}, {1}", v0, v1, v2, v3, v4  -  result: v0, v2, v1


            int a;
            Console.WriteLine("Enter integer number");
                var tmp = Console.ReadLine(); // var - type определяется автоматом, реккоменд майкрософт
            // string to INT  ver.1
                a = int.Parse(tmp); // auto convert string to INT, or trow
            // string to INT ver.2

            // return true/false - ok or NO
            if(    int.TryParse(tmp, a)  ) 
            {Console.WriteLine("Error convert string2int");
            }

            //string to INT ver.3
            a = Convert.ToInt32(tmp);
              




        }
        // программа заканчивается когдла закончился метод Main
    }
}
