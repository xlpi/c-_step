﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Garbage Collector
// Collect() - принудительная сборка
//Collect(int) - принудительная сборка определенного поколения
//Finalize()

// есть также неуправляемая память вне доступа .Net

// ~ ()  - финалийзер

namespace _24.GarbageCollector
{
    class Program
    {
        static void Main(string[] args)
        {
            BaseClass b = new BaseClass();  // закоментиьть эли это
            /////////////////////////////////////////   или это
            using (BaseClass c = new BaseClass())    // только для объектов реализующих IDispose
            {
                Console.WriteLine(c);
            }
            //////////////////////////////////////////
            GC.Collect();

           

        }
    }
}
