﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _24.GarbageCollector
{
    class BaseClass : IDisposable
    {
        bool dispose = false;

        public void Dispose()
        {// освободить неуправляемые ресурсы
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool d)
        {
            if(dispose)
            {
                return;
            }

            if(d)
            {   // удаление явным образом
                // Dispose managment object
                // повызывать методы Dispose наших классов
                Console.WriteLine("Dispose");
            }
            else
            {
                Console.WriteLine("Finalizer");
            }

            dispose = true;

        }

        ~BaseClass() 
        {
            Dispose(false);
        }
    }
}
