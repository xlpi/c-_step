﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23.DiubleLinkedList
{
    class Node<T>
    {
        Node<T> _next;
        Node<T> _previus;
        T _value;

        public Node<T> Next { get { return _next; } set { _next = value; } }
        public Node<T> Previus { get { return _previus; } set { _previus = value; } }
        public T Value { get { return _value; } set { _value = value; } }

        public Node()
        {
            Next = null;
            Previus = null;
            Value = default(T);
        }

        public Node(T value, Node<T> prev, Node<T> next)
        {
            Next = next;
            Previus = prev;
            Value = value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }


    }
}
