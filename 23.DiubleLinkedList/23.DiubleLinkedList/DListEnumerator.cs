﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23.DiubleLinkedList
{
    class DListEnumerator<T>: IEnumerator<T>
    {
        Node<T> _head;
        Node<T> _current = null;
        bool bFirst = true;

        public DListEnumerator(Node<T> head)
        {
            _head = head;
        }

        public bool MoveNext()
        {
            if (bFirst)
            {
                _current = _head;
                bFirst = false;
            }
            else
            {
                if (_current == null)
                {
                    throw new Exception("current == null");
                }
                else
                {
                    _current = _current.Next;
                }
            }
            return _current != null;
        }

        public T Current
        {
            get { return _current.Value; }
        }

        object System.Collections.IEnumerator.Current
        {
            get { return this._current.Value; }
        }

        public void Reset()
        {
            bFirst = true;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
