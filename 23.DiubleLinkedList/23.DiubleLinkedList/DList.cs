﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// http://evilcoderr.blogspot.com/2013/01/doubly-linked-list-c.html
namespace _23.DiubleLinkedList
{
    class DList<T>: ICollection<T> where T: IComparable
    {// ICollection также наследуемся от IEnumarable
        // в каждом Узле - значение, предыдущий и следующий
        Node<T> _head;
        Node<T> _tail;
        int count = 0;


        T Front 
        { 
          get 
          {
              if (_head == null)
              {
                  throw new Exception("Error head");
              }

              return _head.Value;
          }
        } // возвращают значения головы

        T Back
        {
            get
            {
                if (_tail == null)
                {
                    throw new Exception("Error tail");
                }

                return _tail.Value;
            }
        } // возвращают значения хвоста


        public DList()
        {// создать список все нули
           
            count = 0;
                
        }

        public void push_back(T item)
        {
                                        // val  -->prev -->next
            Node<T> newNode = new Node<T>(item, null, _tail);
            if (_tail == null)
            {
                _head = newNode;
            }
            else
            {
                _tail.Next = newNode;
            }

            _tail = newNode;
            count++;

            //1. add node new   //2. new.value = item  //3. new.prev = null;  //4. new.next = _head;
            //5. _head.prev = new.next;  //5. _head = new node;  //6. _перенсти голову   head = new;
                    }

        public void push_front(T item)
        {
            Node<T> newNode = new Node<T>(item, _head, null);
            if (_head == null)
            {
                _tail = newNode;
            }
            else
            {
                _head.Previus = newNode;
            }

            _head = newNode;
            count++;
        }

        public void pop_back() // убрать с конца
        {
            var t = _tail;          // backUp _tail
            _tail = _tail.Previus;  // _tail на 1 назад

            t.Previus = null;       // удалили старую связь backUp _tail.Previus 
            if (_tail != null)      // если 
            {
                _tail.Next = null;
            }
            else
            {
                _head = null;
            }
            count--;

        }

        public void pop_front()
        {// перепроверить......

            //var h = _head;
            //_head = _head.Next;

            //h.Previus = null;

        }

        public void Add(T item)
        {
            this.push_back(item);
        }


        public void Clear()
        {

        }

        public bool Contains(T item)
        {
            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
        }

        public int Count
        {
            get{return count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            var c = _head;
            while (c != null)
            {
                if (c.Value.CompareTo(item) == 0) { break; }

                c = c.Next;
            }

            if (c == null)
            {
                return false;
            }
            else
            {
                if (c.Previus == null)
                {
                    _head = c.Next;
                }
                else
                {
                    c.Previus.Next = c.Next;
                }

                if (c.Next == null)
                {
                    _tail = c.Previus;
                }
                else c.Next.Previus = c.Previus;

                c.Next = null;
                c.Previus = null;
                c = null;
            }

            count--;
            return true;
        }

        public IEnumerator<T> GetEnumerator()
        {// объект который сможет перебирать узлы хранящийся в своем списке, Current, Next
            return new DListEnumerator<T>(_head);
        }


        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return new DListEnumerator<T>(_head);
        }

        public override string ToString()

        {
            if (_head.Previus == null && _tail.Next == null)
            {
                return "HEAD: value=" + Front + "; prev=null; next=" + _head.Next +
                " --- TAIL: value=" + Back + "; prev=" + _tail.Previus + "; next=null";
            }
            else
            {
                return "HEAD: value=" + Front + "; prev=" + _head.Previus + "; next=" + _head.Next +
                " --- TAIL: value=" + Back + "; prev=" + _tail.Previus + "; next=" + _tail.Next;
            }
        }
    }
}
