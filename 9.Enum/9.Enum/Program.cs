﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9.Enum
{
    enum ArriValStatus
    {
        Unknown = -3,
        Late = -1,
        OnTime = 0,
        Early = 1
    }

    enum Sax
    {
        male,
        female
    }

    // flags - может быть в enum
    enum Sex
    {
        male,
        female
    }





    class Program
    {

        // eNUm in .NET
        // in class or in namespace

     
        // by def first elem = 0; second +1
        static void Main(string[] args)
        {
    

        ArriValStatus ab = ArriValStatus.Unknown;
        int a = (int)ab; // convert to int
        Console.WriteLine(a);

            // any emun наследуется от Enum - abstract class

        var str = System.Enum.GetNames(typeof(ArriValStatus)); // return names of constants
            foreach (var item in str)
	{
Console.WriteLine(item);		 
	}


            var str1 = System.Enum.GetValues(typeof(ArriValStatus)); // return value from enum

            foreach (var item in str1)
            {
                Console.WriteLine((int)item);
            }
            Console.WriteLine(
              );
            Console.WriteLine();


            Console.WriteLine("Enter sax: ?");
            var sax = Console.ReadLine();
            Sax s;
            try
            {
                // isDefined  - применить для того чтобы в enum не попало число
                s = (Sax)System.Enum.ToObject(typeof(Sax), Convert.ToInt32(sax));
                s = (Sax)System.Enum.Parse(typeof(Sax), sax, true);  // true - игнорирование регистра
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                s = Sax.male;
            }

            // flags - может быть в enum  -  битовое поле
            Sex s1 = Sex.male | Sex.female;
           




        }
    }
}
