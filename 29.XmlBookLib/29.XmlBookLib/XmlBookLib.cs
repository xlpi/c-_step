﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
// создать xml bookstore
//<bookstore>
//    <book>
//        <title></title>
//        <author></author>
//        <price></price>
//        <year></year>
//    </book>
//</bookstore>

//1. Ввести автора - вывести список его книг
//2. Ввести год, вывести все книги год которых меньше
//3. Список книг отсортированный по цене

//https://msdn.microsoft.com/en-us/library/d271ytdx(v=vs.110).aspx
namespace _29.XmlBookLib
{
    class XmlBookLib
    {
        static void Main(string[] args)
        {
            XmlDocument doc = new XmlDocument();

            var pi = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            XmlElement root = doc.CreateElement("bookstore");
            doc.AppendChild(root);

            var book = doc.CreateElement("book");
            root.AppendChild(book);

            var title = doc.CreateElement("title");
            title.InnerText = "Как закалялась сталь";
            book.AppendChild(title);

            var author = doc.CreateElement("author");
            author.InnerText = "Николай Островский";
            book.AppendChild(author);

            var price = doc.CreateElement("price");
            price.InnerText = "250.00 UAH";
            book.AppendChild(price);

            var year = doc.CreateElement("year");
            year.InnerText = "1985";
            book.AppendChild(year);

            ////////////////////////////////////////////////////
            var book2 = doc.CreateElement("book");
            root.AppendChild(book2);

            var title2 = doc.CreateElement("title");
            title2.InnerText = "Как на хуторе...";
            book2.AppendChild(title2);

            var author2 = doc.CreateElement("author");
            author2.InnerText = "Моревко";
            book2.AppendChild(author2);

            var price2 = doc.CreateElement("price");
            price2.InnerText = "60.00 UAH";
            book2.AppendChild(price2);

            var year2 = doc.CreateElement("year");
            year2.InnerText = "2003";
            book2.AppendChild(year2);

            ////////////////////////////////////////////////////
            var book3 = doc.CreateElement("book");
            root.AppendChild(book3);

            var title3 = doc.CreateElement("title");
            title3.InnerText = "Близ Диканьки";
            book3.AppendChild(title3);

            var author3 = doc.CreateElement("author");
            author3.InnerText = "Неизвестный";
            book3.AppendChild(author3);

            var price3 = doc.CreateElement("price");
            price3.InnerText = "183.00 UAH";
            book3.AppendChild(price3);

            var year3 = doc.CreateElement("year");
            year3.InnerText = "1815";
            book3.AppendChild(year3);

            ////////////////////////////////////////////////////
            var book4 = doc.CreateElement("book");
            root.AppendChild(book4);

            var title4 = doc.CreateElement("title");
            title4.InnerText = "Большевики";
            book4.AppendChild(title4);

            var author4 = doc.CreateElement("author");
            author4.InnerText = "Николай Островский";
            book4.AppendChild(author4);

            var price4 = doc.CreateElement("price");
            price4.InnerText = "230.00 UAH";
            book4.AppendChild(price4);

            var year4 = doc.CreateElement("year");
            year4.InnerText = "1983";
            book4.AppendChild(year4);
            ///////////////////////////////////////////////////
            doc.Save("MyBookLib.xml");


            doc.Load("MyBookLib.xml");


            while (true)
            {
                Console.WriteLine("1 - find book list by book Author");
                Console.WriteLine("2 - input year to print books wich year more early");
                Console.WriteLine("3 - print book list sorted by price");
                string choose = Console.ReadLine();

                switch(choose){
                    case "1"://1. Ввести автора - вывести список его книг
                        {
                            Console.WriteLine("Enter author to find him books");
                            String findAuthor = Console.ReadLine();
                            XmlNodeList xml = doc.SelectNodes("//book[author= '" + findAuthor + "']");

                            foreach (XmlNode list in xml)
                            {
                                // чтоб вывести отдельно все дочерние элементы XmlNode tmp = doc.SelectSingleNode("//title");

                                Console.WriteLine(list.InnerText);
                                //Console.WriteLine(list.Name + ":" + list.InnerText); // лучше и так работает
                            }
                        } break;
                    case "2"://2. Ввести год, вывести все книги год которых меньше
                        {
                            Console.WriteLine("Enter year to print books wich year more early");
                            String findYear = Console.ReadLine();
                            XmlNodeList xml = doc.SelectNodes("//book[year<'" + findYear + "']");
                            foreach (XmlNode list in xml)
                            {
                                Console.WriteLine(list.InnerText);
                            }

                        } break;
                    case "3"://3. Список книг отсортированный по цене
                        {
                            
                        }break;
                    default: return;


            }
            }
        }
    }
}
