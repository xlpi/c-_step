﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _17.Interfaces
{
    // у интерфейса два способа реализ ЯВНО и неЯВНО
    // все мтоды виртуальные, доступные всем
    // интерфейс - это просто набор пустых методов для дальнейшей конкретной реализации
    // соглашение о том как Студент.Прыгать() - будет принимать параметры.....

    interface IHuman
    {
        // то что должны быть у объекта который будет использовать или реализовывать интерфейс
        void Work();
        String Name { get; set; }
    }

    interface IWorker
    {
        void Work();
    }

    struct Student : IHuman, IWorker
    {
        // - эта структура при передачи в мейне в метод не будет упаковываться и распаковываться
        public String Name { get; set; }

        void IHuman.Work()
        {
            Console.WriteLine("IHuman.Work()");
        }

        void IWorker.Work()
        {
            Console.WriteLine("IWorker.Work()");
        }

        static void SleepWith(IHuman h)
        {
            h.Name = "Петя";
            Console.WriteLine(h.Name);
        }
     
    };

    class Group : IEnumerator, 
    {
        LIst<IStudent> s;
        s[0].Add(); // метод который будет реализован в интерфесе, который еще не реализова
        // и будет реализован в класссе Student

    }


    class InterfacesMain
    {
        static void Main(string[] args)
        {
            Student s;
            s.Name = "Вася";
          //  Student.SleepWith(s);  not work
           //  Console.WriteLine(Student.Name); not work

        }
    }
}
