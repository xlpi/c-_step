﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5.String
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            String // .NET
                string; // C#
            string s = "One";
            string s1 = "One"; // ссылка на одно и то же место
            s1 = s1 + '1'; // concatination

            // constructor
            s1 = new string(Char[]
                Char32[]

            Char* // не безопасный тип
            SByte* // массив байт


            // Properties
            s1.Length[]; //
            s1.Clone
                CompareTo(string); // 1 if >, or 0 if <    abc<abcd , abc< bcd
            EndsWith(string); // заканчивается ли нашастрока такой строкой
                StartWith(string);// начинается ли строка этой подстрокой  можно указать тест на КеЙс

            // Method  NonStatic
            s2 = s.Insert(s1,5);// результат всегда в возврате и всегда создается новая строка
            NormalizeTo(); // не нормализированная - это не в Юникоде. Если один символ нельзя представить как один символ Юникода - это не нормализированная строка
           s3 = s.PadLeft(); // выравнивает по левому краю и добавляет пробелы
            s3 = s.Remove(); // удаляет из строки указанные символы
            s3 = s.Split(); // разделитель, строка
                string s = "One, , Two, Three";
            s3 = s.Split(new char[](',',' '),      )   ; ///     separator, StrinSplitOption   -  по умолчанию не удаляет два подряд сепаратора
            s3 = "OneTwoThree";
            s.Substring();
            s.ToCharArray();
            s.ToLower(); // используя культуру можно указать - если маленькая буква выглядит совсем подругому от Большой, по умолчанию беруться регионал настр 
            s.ToUpper();
            s.TrimStart(); // удал пробел, табы, перевод строки
            s.TrimEnd();
            s.Trim();


            // static Method
            Comparer(s1,s1); // register, culture
            Concat(s1,s2); //
            Copy(s1,s2); 
            Equals(s1,s2);
            Format // указываем 

              string  s =   string.Format("str Format", ... )
            "{index[,length]:[строкаФормата] }

                simple:
             */
            int a = 2; // min количнство знаков необходимое - по правой + по левой выровнять
              //                                0-индекс,3 -выравнивание(+/-) E -         1- индекс,5-выравнивание   
          //  выравнивание - скольок знакомест минимально выделяем для вывода параметра
           //  отрицательное - по левой стороне, + - по правой
            string s = string.Format("Квадрат {0,3:E} равен {1,5:N}", a, a * a);
            Console.WriteLine(s);
            string s1 = string.Format("Квадрат {0,-3:E} равен {1,5:N}", a, a * a);
            Console.WriteLine(s);
            string s2 = string.Format("Квадрат {0,3:E} равен {1,-5:N}", a, a * a);
            Console.WriteLine(s);
            string s3 = string.Format("Квадрат {0,3:E} равен {1,5:N}", a, a * a);
            Console.WriteLine(s);

            // есть форматы для даты - Украина
            // d - 21.02.2015
            // D - Субота, 21 лютого 2015
            // f - краткий формат +времени - Субота, 21 лютого 2015 17:28
            // F - краткий формат +времени - Субота, 21 лютого 2015 17:28:13
            // g - simple time template - 21.02.2015 17:28
            // G - full time template - 21 лютого 2015 17:28:13
            // m - month 21 лютого
            // M - month 
            // t - time 17:28
            // T - time 17:27:15

            // настраиваевы формат даты и времени

            // d - day, month
            // dd - 
            // ddd - + назв недели
            // dddd - назв дня недели полное
            // f - 10е доли секунды
            // ff - 100е
            // fff - 1000
            // G - до н.е или н.е
            // h - часы 1-12
            // hh 0-12
            // H - 0-23
            // HH - 0,1   0,2
            // m - min
            // mm - min
            // M - month
            // S - sec
            // Y - year
            // YY - 
            // YYY
            // YYYY
            // z - сдвиг от UTC без 0
            // 
        }
    }
}
