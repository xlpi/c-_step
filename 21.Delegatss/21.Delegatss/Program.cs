﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _21.Delegatss
{
    // degats хранят ссылку на метод, его можно вызывать как метод
    // используется для некоего макроса действий
    // или для событий


    // обработчик Опер
    public delegate int OperationHandler(int a, int b);

    delegate void EventHandler (Object sender, EventArgs e); // соглашение о реализации деллегатов, у sender только спрашиваем тип Авто или нет и реализуем






    class Program
    {

        
        static int S1(int a, int b)
        {
            return a + b;
        }

        int S2(int a, int b)
        {
            return a * b;
        }

        static void Main(string[] args)
        {
            // anonime methods  was early  сейчас исп лямбда
            int count = 0;

            Automobile b = new Automobile();

            //anonime method
            SpeedChangedHandler d = delegate(Automobile z)
            {
                count++; //see on one level UP
            };

            b.SpeedChange += d; // деллигат 

            b.Acceleration();
            b.Break();

            /////////////////////////////////////////////////////////
            // вместо анонимного можем исп лямбда выражение
            // исп когда метод 1-2 строки
            b.SpeedChange += (y) => { count++; };






            ///////////////////////////////////////////////////////////

            OperationHandler h = new OperationHandler(S1);
           // Console.WriteLine(h(2,3));

            // в новой версии C#
            h = S1; // compil уже знает что нужно создать делегат
            //Console.WriteLine(h(3,4));

            h += (new Program()).S2; // добавляем в делеггат еще одну ссылку на метод не статический, поэтому создаем new Program
           // int c = h(3,3);
           // Console.WriteLine(c);// порядок выполнение не гарантируется, вообще это внутренне усройство и НЕ реккомендуется к использованию

            

            //h -= S1; // удаляем ссылку S1 в деллегате
             
            // Properties
           // h.Method; // return info o method
           // h.Target; // return instance текущего деллегата

            // Method деллегатов
          //  h.GetInvocationList(); // return list of methods in delegate

            bool exec = true;
            // берем список всех методов деллегатеи вызываем по очереди
            foreach (OperationHandler item in h.GetInvocationList())
            {
                if (exec)
                {
                    Console.WriteLine(item(3, 3)); // усли нет ссылки на объект в котором метод то пропускаем
                }

                exec = !exec;
            }


           // Console.ReadKey();
            ///////////////////////////////////////////////////////////

            // гаишник и скорость авто
            Automobile a = new Automobile();

            // подписка на событие о изменении скорости авто   делегируем полномочия считывать скорость
            a.SpeedChange += a_SpeedChange; // после += нажимаем tab tab -> студия генерит метод a_SpeedChange(Automobile a)

            Console.WriteLine("Speed UP");
            a.Acceleration(); // теперь создается событие
            Console.WriteLine("Speed UP");
            a.Acceleration(); // теперь создается событие
            Console.WriteLine("Speed UP");
            a.Acceleration(); // теперь создается событие
            Console.WriteLine("Speed UP");
            a.Acceleration(); // теперь создается событие
            Console.WriteLine("Speed Down");
            a.Break(); // теперь создается событие
            Console.WriteLine("Speed Down");
            a.Break(); // теперь создается событие
            Console.WriteLine("Speed Down");
            a.Break(); // теперь создается событие
            Console.WriteLine("Speed Down");
            a.Break(); // теперь создается событие
            Console.WriteLine("Speed Down");
            a.Break(); // теперь создается событие

            // сколько всего менялась скорость у b
            Console.WriteLine("Count:{0}",count);

            Console.ReadKey();
       
        }

        static void a_SpeedChange(Automobile a)
        {
            Console.WriteLine("Current speed of avto: {0}",a.Speedcurrent);
        }


        
    }
}
