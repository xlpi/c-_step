﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _21.Delegatss
{

    // создадим потом событие чтобы всем кому надо говорить о нашей текущей скорости
    delegate void SpeedChangedHandler(Automobile a);  // see in only namespace

    class Automobile
    {
        public event SpeedChangedHandler SpeedChange;

        public int Speedcurrent { get; private set; }
        public void Acceleration()
        {
            Speedcurrent++;

            // метод вызывающий событие
            RaiseSpeedChanged();
        }

        public void Break()
        {
            Speedcurrent--;
            // метод вызывающий событие
            RaiseSpeedChanged();
        }

        protected void RaiseSpeedChanged()
        {
            // так как много подписчиков и чтоб ничего не совпало пишем так
            var handler = SpeedChange;
            if (handler != null)
            {
                handler(this);
            }
        }
    }
}
