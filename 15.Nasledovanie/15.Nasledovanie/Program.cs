﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15.Nasledovanie
{
    // наследование всегда public
    // множественного наследования нет, только через интерфейсы

    class Human
    {
        /*
        public 
        private // only in this class
        internal // public in package
        protected internal // доступен наслендникам вне сборки тоже

         * */
 
        

        public String Name
        {
            get;
            set;
        }
         public String Surname
        {
            get;
            set;
        }
         public DateTime Birthday
        {
            get;
            set;
        }

        // constructor
        public Human(String name, String surname, DateTime birthday)
        {
           
        }

        public  virtual void Work()
        {
            // переопределим в Student
            Console.WriteLine("Human .Work");
        }


      

        public virtual void Sleep()
        {
            
            Console.WriteLine("Human.Sleep");
        }
    }

    class Student : Human
    {
        public Student(String name, String surname, DateTime birthday)
            : base(name, surname, birthday)
        {

        }

       //  base();   // аналог super

        public override void Work()
        {
            
         
           Console.WriteLine("Student.Work");
        }

        public new void Sleep()
        {
            Console.WriteLine("Student.Sleep");

        }




    }



    class Program
    {
        static void Main(string[] args)
        {
            Student s = new Student("Вася", "Петров", DateTime.Now);
            s.Work();
            s.Sleep();


            Human h = s;
            h.Work();
            h.Sleep(); // Sleep() from Human only

        }
    }
}
