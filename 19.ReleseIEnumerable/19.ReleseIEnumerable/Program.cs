﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19.ReleseIEnumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            Human[] h = new Human[] {
               new Human(){Name = "", Surname = ""},
               new Human(){Name = "", Surname = ""},
               new Human() {Name = "", Surname = ""}
           };

            People p = new People(h); // our collection
            // use our IEnumerator interface - перебираем коллекцию людей
            foreach(var i in p)
            {
                Console.WriteLine(i.Name, i.Surname);
            }

        }
    }
}
