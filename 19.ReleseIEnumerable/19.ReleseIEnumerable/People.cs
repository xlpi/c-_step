﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19.ReleseIEnumerable
{
    class People: IEnumerable
    {
        private Human[] _people;
        public People(Human[] p)
        {
            _people = new Human[p.Length];
            p.CopyTo(_people, 0);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public PeopleEnum GetEnumerator()
        {
            return new PeopleEnum(_people);
        }
    }


}
