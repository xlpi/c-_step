﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19.ReleseIEnumerable
{
    class PeopleEnum : IEnumerator
    {
        private Human[] _p;
        private int position = -1;

        public PeopleEnum(Human[] p)
        {
            _p = p;
        }


        // press Ctrl+.   on IEnumerator -> using SystemCollection, again press Imliment....
        public Human Current
        {
            get
            {
                try
                {
                    return _p[position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }


            }
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public bool MoveNext()
        {
            position++;
            return (position < _p.Length);

        }

        public void Reset()
        {
            position = -1;
        }
    }
}
